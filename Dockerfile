FROM httpd:2.4
COPY . /usr/local/apache2/htdocs
MAINTAINER Miriam Jost <miriam.jost@autismuslink.ch>
EXPOSE 80